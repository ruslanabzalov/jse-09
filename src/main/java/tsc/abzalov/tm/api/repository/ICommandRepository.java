package tsc.abzalov.tm.api.repository;

import tsc.abzalov.tm.model.Command;

public interface ICommandRepository {

    Command[] getCommands();

    String[] getCommandNames();

    String[] getCommandArgs();

}
