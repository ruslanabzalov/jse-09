package tsc.abzalov.tm.bootstrap;

import tsc.abzalov.tm.api.controller.ICommandController;
import tsc.abzalov.tm.api.repository.ICommandRepository;
import tsc.abzalov.tm.api.service.ICommandService;
import tsc.abzalov.tm.controller.CommandController;
import tsc.abzalov.tm.repository.CommandRepository;
import tsc.abzalov.tm.service.CommandService;

import java.util.Scanner;

import static tsc.abzalov.tm.constant.ApplicationArgumentConst.*;
import static tsc.abzalov.tm.constant.ApplicationArgumentConst.ARG_ARGUMENTS;
import static tsc.abzalov.tm.constant.ApplicationCommandConst.*;

public class CommandBootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    public void run(String... args) {
        System.out.println();
        if (areArgExists(args)) return;

        System.out.println("****** WELCOME TO TASK MANAGER APPLICATION ******");
        System.out.println("Please, use \"help\" command to see all available commands.\n");

        final Scanner scanner = new Scanner(System.in);
        String command;
        while (true) {
            System.out.print("Please, enter your command: ");
            command = scanner.nextLine();
            System.out.println();
            if (command == null || command.isEmpty()) continue;
            processCommand(command);
        }
    }

    private boolean areArgExists(String... args) {
        if (args == null || args.length == 0) return false;

        final String arg = args[0];
        if (arg == null || arg.isEmpty()) return false;

        processArg(arg);
        return true;
    }

    private void processArg(String arg) {
        switch (arg) {
            case ARG_HELP:
                commandController.showHelp();
                break;
            case ARG_INFO:
                commandController.showInfo();
                break;
            case ARG_ABOUT:
                commandController.showAbout();
                break;
            case ARG_VERSION:
                commandController.showVersion();
                break;
            case ARG_COMMANDS:
                commandController.showCommandNames();
                break;
            case ARG_ARGUMENTS:
                commandController.showCommandArgs();
                break;
            default:
                commandController.showError(arg, true);
        }
    }

    private void processCommand(String command) {
        switch (command) {
            case CMD_HELP:
                commandController.showHelp();
                break;
            case CMD_INFO:
                commandController.showInfo();
                break;
            case CMD_ABOUT:
                commandController.showAbout();
                break;
            case CMD_VERSION:
                commandController.showVersion();
                break;
            case CMD_COMMANDS:
                commandController.showCommandNames();
                break;
            case CMD_ARGUMENTS:
                commandController.showCommandArgs();
                break;
            case CMD_EXIT:
                commandController.exit();
            default:
                commandController.showError(command, false);
        }
    }
}
