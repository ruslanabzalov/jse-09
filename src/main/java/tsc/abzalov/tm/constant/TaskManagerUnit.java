package tsc.abzalov.tm.constant;

public enum TaskManagerUnit {

    COMMAND("Command"), ARGUMENT("Argument");

    private final String name;

    TaskManagerUnit(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

}
