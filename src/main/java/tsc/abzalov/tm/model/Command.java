package tsc.abzalov.tm.model;

public class Command {

    private String name = "";

    private String arg = "";

    private String description = "";

    public Command() {
    }

    public Command(String name, String arg, String description) {
        this.name = name;
        this.arg = arg;
        this.description = description;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArg() {
        return this.arg;
    }

    public void setArg(String arg) {
        this.arg = arg;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        final String emptyString = "";
        final String correctName = (this.name == null || this.name.isEmpty()) ? emptyString : this.name;
        final String correctArg = (this.arg == null || this.arg.isEmpty())
                ? emptyString : (!correctName.isEmpty())
                ? " [" + this.arg + "]" : "[" + this.arg + "]";
        final String correctDescription = (this.description == null || this.description.isEmpty())
                ? emptyString : (!correctArg.isEmpty() || !correctName.isEmpty())
                ? ": " + this.description + emptyString : this.description;
        return correctName + correctArg + correctDescription;
    }

}
